<?php
	$now = date('Y-m-d H:i:s');
	$warning 		= false; 	#true เพื่อแสดงข้อความเตือน
	$stop_process 	= false;	#true แสดงข้อความเตือน และหยุดการทำงานของโปรแกรม

	// nuy@2015-01-22
	$download		= true; 	#true เพื่อแสดงข้อมูลให้ดาวน์โหลดเวอร์ชั่น firefox
	$path_download	= '../ERP_firefox_download';

	if($download == true){
		$msg_ubuntu		= '<span>Firefox for Linux</span><br/>';
		$msg_ubuntu		.= '<a href="'.$path_download.'/firefox-3.6.19.tar.bz2">Firefox version 3.6.19</a>';
		$msg_ubuntu		.= ' | ';
		$msg_ubuntu		.= '<a href="'.$path_download.'/firefox22.tar.bz2">Firefox version 22.0</a>';

		$msg_windows	= '<span>Firefox for Windows</span><br/>';
		$msg_windows	.= '<a href="'.$path_download.'/Firefox Setup 3.6.19.exe">Firefox version 3.6.19</a>';
		$msg_windows	.= ' | ';
		$msg_windows	.= '<a href="'.$path_download.'/Firefox Setup 22.0b6.exe">Firefox version 22.0</a>';

		$tag_download	= '<style type="text/css">';
		$tag_download	.= '#divLDownload {
								text-align: center;
								margin: 10px auto;
								line-height: 19px;
								width: 865px;
							}

							#divLDownload > div {
								width: 50%;
								display: inline-block;
							}

							#divLDownload > div > span {
								font-size: 16px;
								font-weight: bold;
							}

							#divLDownload > div > a:link, #divLDownload > div > a:visited { 
							    color: (internal value);
							    text-decoration: underline;
							}

							#divLDownload > div > a:link:active, #divLDownload > div > a:visited:active { 
							    color: (internal value);
							}';
		$tag_download	.= '</style>';

		$tag_download	.= '<div id="divLDownload">';
		$tag_download	.= '<div>'.$msg_ubuntu.'</div>';
		$tag_download	.= '<div>'.$msg_windows.'</div>';
		$tag_download	.= '</div>';

		echo $tag_download;
	}
	// nuy@2015-01-22

	/*$msg 	= '<font color="white">เลือก path ชั่วคราว จาก  icchecktime2  table place FIELD path  &nbsp;&nbsp;<br> หากไม่มี link ให้เพิ่มใน place ชั่วคราวก่อน  (เวลาขณะนี้ <font color="yellow">'.date('H:i:s').'</font>) </font>';	#ข้อความแจ้งเตือน
	
	$warning_msg 	= "<table width=\"100%\" cellspacing=\"0\" border=\"0\" valign=\"BOTTOM\" class=\"mattblackmenu\" style=\"border-collapse:collapseheight:5px;z-index:10000;background:#414141;width:100%\">
		<tbody>
			<tr>
				<td>
					<div style=\"width:100%;text-align:center;\" id=\"ddtopmenubar\">
						<ul>
							$msg
						</ul>
					</div>
				</td>
			<td></td>
			</tr>
		</tbody>
	</table>";*/
	// $msg = 'เลือก path ชั่วคราว จาก  icchecktime2  table place FIELD path  &nbsp;&nbsp; <br> หากไม่มี link ให้เพิ่มใน place ชั่วคราวก่อน  (เวลาขณะนี้ <font color="blue">'.date('H:i:s').'</font>)';
	
	$msg   = '<p>เนื่องจากมีการอัพเดตเวอร์ชั่นและติดตั้งโปรแกรม EasyERP หากพบข้อผิดพลาดรบกวนแจ้ง   แผนกพัฒนาระบบงาน</p>';
	// $msg  = '<p>เนื่องจากจะมีการอัพเดตเวอร์ชั่นและติดตั้งโปรแกรม Easy Sale และ Easy Service ( เพื่อเตรียมติดตั้ง ERP )</p>';
	// $msg .= '<p><h2>ในวันที่ <b><i><u>31 ตุลาคม 2555 ตั้งแต่เวลา 18.00 น. เป็นต้นไป</u></i><b></h2></p>';
	// $msg .= '<p>จึงขอความร่วมมืองดใช้โปรแกรมในวันและเวลาดังกล่าว ขอบคุณค่ะ</p>';
	
	
	$warning_msg = "<div id=\"note\" style=\"width:825px;\">
						<div class=\"note_txt\"><h3>:: Note :: (เวลาขณะนี้ <font color='blue'>".date('H:i:s')."</font>) ขออภัยในความไม่สะดวก</h3></div>
						<div style=\"color:#CC0000;\">
							$msg
						</div>
					</div>";
	
	if(isset($warning)){ echo $warning_msg; }
	
	//$alarm_time = '2012-04-20 16:30:00';
	if(isset($alarm_time) && $now > $alarm_time){
	//$warning = true; 	#true เพื่อแสดงข้อความเตือน
	}

	//$set_deny = array( 'test', 'ic', 'v039', 'easysale_report' , 'easysale' );	//ไดเรกทอรี่ที่จะจำกัดสิทธิ์การใช้ SESSION เดียวกันกับระบบจริง  // ถ้าใช้งานจริง ลบ easysale ออก
?>