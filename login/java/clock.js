
var maxnumhours = 23;
var maxnummins = 59;
var maxnumsecs = 60;
var maxmilisecs = 999;

$(document).ready(function() {
	updateClock();
	setInterval('updateClock()', 250 );
});
function updateClock ( )
{
	var currentTime = new Date ( );
	var currentHours = currentTime.getHours();
	var currentMinutes = currentTime.getMinutes();
	var currentSeconds = currentTime.getSeconds();
	var currentMiliSeconds = currentTime.getMilliseconds();
	var rounded = currentSeconds + (currentMiliSeconds / maxmilisecs);
	rednum = (Math.round(255 * ((currentHours) / maxnumhours)));
	rednum100 = (Math.round(100 * ((currentHours) / maxnumhours)));
	greennum = (Math.round(255 * ((currentMinutes) / maxnummins)));
	greennum100 = (Math.round(100 * ((currentMinutes) / maxnummins)));

	// Leading Zeros
	currentHours = ( currentHours < 10 ? "0" : "" ) + currentHours;
	currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
	currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
	jQuery("#clock").html("<span id='hours'>"+ currentHours + "</span>:<span id='minutes'>" + currentMinutes + "</span>:<span id='seconds'>" + currentSeconds + '</span>');
}