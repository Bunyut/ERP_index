<?php
if( !defined( 'MAX_PAGES' ) )
  define( 'MAX_PAGES', 10 );

if( !defined( 'P_PREFIX' ) )
  define( 'P_PREFIX', '' );

if( !defined( 'LANG_YES_SHORT' ) )
  define( 'LANG_YES_SHORT', 'Tak' );

if( !defined( 'LANG_NO_SHORT' ) )
  define( 'LANG_NO_SHORT', 'Nie' );

if( !defined( 'MAX_STR_LEN' ) )
  define( 'MAX_STR_LEN', 40 );

function throwYesNoSelect( $nr ){
  for( $l = 0; $l < 2; $l++ ){
    if( is_numeric( $nr ) && $nr == $l ) 
      $select[$l] = 'selected="selected"';
    else		
      $select[$l] = '';
  } // end for

  $option =  '<option value="1" '.$select[1].'>'.LANG_YES_SHORT.'</option>';
  $option .= '<option value="0" '.$select[0].'>'.LANG_NO_SHORT.'</option>';

  return $option;
} // end function throwYesOrNoSelect
if(!function_exists('checkTemplateDir')){
  function checkTemplateDir($dir){
    if(!is_dir($dir)){
      if(mkdir($dir)){
        if(chmod($dir,0777)){
          return $dir;
        }}
      return false;
    }else{
      return $dir;
    }}
}
function throwYesNoBox( $sBoxName, $iYesNo = 0 ){
  if( $iYesNo == 1 )
    $sChecked = 'checked="checked"';
  else
    $sChecked = null;

  return '<input type="checkbox" '.$sChecked.' name="'.$sBoxName.'" value="1" />';
} // end function throwYesNoBox

function throwYesNoTxt( $nr = false ){
  if( $nr == 1 )
    return LANG_YES_SHORT;
  else
    return LANG_NO_SHORT;
} // end function throwYesNoTxt





function changeTxt( $txt, $opcja = '' ){

  if( eregi( 'tag', $opcja ) )
    $txt = changeHtmlEditorTags( $txt );

  if( eregi( 'h', $opcja ) )
    $txt = htmlspecialchars( $txt );

  $txt = changeSpecialChars( $txt );
  if( eregi( 'nss', $opcja ) ){
    $txt = ereg_replace( "\n", '', $txt );
    $txt = ereg_replace( '\|n\|', "<br>"."\n" , $txt );
  }
  if( !eregi( 'nds', $opcja ) )
    $txt = ereg_replace( '"', "&quot;", $txt );

  if( eregi( 'sl', $opcja ) )
    $txt = addslashes( $txt );
  else
    $txt = stripslashes( $txt );

  $txt = ereg_replace( "\r", '', $txt );

  if( eregi( 'len', $opcja ) )
    $txt = checkLengthOfTxt( $txt );

  if( eregi( 'ns', $opcja ) ){
    $txt = ereg_replace( "\n", '', $txt );
    $txt = ereg_replace( '\|n\|', "" , $txt );
  }



  if( eregi( 'nl', $opcja ) ){
    $txt = ereg_replace( "\n", '', $txt );
    $txt = ereg_replace( '\|n\|', "\n" , $txt );
  }
  else{
    if( eregi( 'br', $opcja ) )
      $txt = ereg_replace( "\n", '<br />', $txt );
    else
      $txt = ereg_replace( "\n", '|n|', $txt );
  }

  if( eregi( 'space', $opcja ) )
    $txt = ereg_replace(' ','',$txt);

  return $txt;
} // end function changeTxt

function changeMassTxt( $aData, $sOption = null ){
  $iParams = func_num_args( );
  if( $iParams > 2 ){
    $aParam = func_get_args( );
    for( $i = 2; $i < $iParams; $i++ ){
      $aData[$aParam[$i][0]] =    changeTxt( $aData[$aParam[$i][0]], $aParam[$i][1] );
      $aDontDo[$aParam[$i][0]] =  true;
    } // end for
  }
    
  foreach( $aData as $mKey => $mValue )
    if( !isset( $aDontDo[$mKey] ) && !is_numeric( $mValue ) && !is_array( $mValue ) )
      $aData[$mKey] = changeTxt( $mValue, $sOption );
  return $aData;
} // end function changeMassTxt

function changeMassFloat( $aData, $iAfterDot = 2 ){
    
  foreach( $aData as $mKey => $mValue )
    if( is_numeric( $mValue ) )
      $aData[$mKey] = sprintf( '%01.'.$iAfterDot.'f', $mValue );

  return $aData;
} // end function changeMassFloat

function checkLengthOfTxt( $txt ){
  return wordwrap( $txt, MAX_STR_LEN, " ", 1 );
} // end function checkLengthOfTxt

function checkLength( $txt, $length = 3 ){
  if( strlen( changeTxt( $txt, 'hBrSpace' ) ) > $length )
    return true;
  else
    return false;
} // end function checkLength

function dateToTime( $date, $time = null, $dateFormat = 'ymd', $sepDate = '-', $sepTime = ':' ){
  
  if( $dateFormat == 'dmy' ){
    $y	= 2;
    $m	= 1;
    $d	= 0;
  }
  else{
    $y	= 0;
    $m	= 1;
    $d	= 2;
  }

  $exp =		@explode( $sepDate, $date );
  $year =		$exp[$y];
  $month =	sprintf( '%01.0f', $exp[$m] );
  $day =		sprintf( '%01.0f', $exp[$d] );

  if( empty( $time ) )
    $time = '00'.$sepTime.'00'.$sepTime.'00';
  
  $exp =		@explode( $sepTime, $time );
  $hour=		sprintf( '%01.0f', $exp[0] );
  $minute=	sprintf( '%01.0f', $exp[1] );

  if( count( $exp ) == 3 )
    $second=	sprintf( '%01.0f', $exp[2] );
  else
    $second=	0;

  return @mktime( $hour, $minute, $second, $month, $day, $year );
} // end function dateToTime

function countPages( $iMax, $iMaxPerPage, $iPage, $sAddress, $sSeparator = '|', $iMaxPagesPerPage = MAX_PAGES ){

  $iSubPages= ceil( $iMax / $iMaxPerPage ); 
  $sPages   = null;
  
  if( $iSubPages > $iPage ) 
    $iNext = 1; 
  else  
    $iNext = 0; 

  $iMax = ceil( $iPage + ( $iMaxPagesPerPage / 2 ) );
  $iMin = ceil( $iPage - ( $iMaxPagesPerPage / 2 ) );
  if( $iMin < 0 )
    $iMax += -( $iMin );
  if( $iMax > $iSubPages )
    $iMin -= $iMax - $iSubPages;

  $l['min'] = 0;
  $l['max'] = 0;
  for ( $i = 1; $i <= $iSubPages; $i++ ) { 
    if( $i >= $iMin && $i <= $iMax ) {
      if ( $i == $iPage ) 
        $sPages .= $sSeparator.' <strong>'.$i.'</strong> '; 
      else 
        $sPages .= $sSeparator.' <a href="?p='.P_PREFIX.$sAddress.'&amp;iPage='.$i.'">'.$i.'</a> '; 
    }
    elseif( $i < $iMin ) {
      if( $i == 1 )
        $sPages .= $sSeparator.' <a href="?p='.P_PREFIX.$sAddress.'&amp;iPage='.$i.'">'.$i.'</a> '; 
      else{
        if( $l['min'] == 0 ){
          $sPages .= $sSeparator.' ... '; 
          $l['min'] = 1;
        }
      }
    }
    elseif( $i > $iMin ) {
      if( $i == $iSubPages ){
        $sPages .= $sSeparator.' <a href="?p='.P_PREFIX.$sAddress.'&amp;iPage='.$i.'">'.$i.'</a> '; 
      }
      else{
        if( $l['max'] == 0 ){
          $sPages .= $sSeparator.' ... '; 
          $l['max'] = 1;
        }
      }
    }
  } // end for
  $sPages .= $sSeparator;

  return $sPages;
} // end function countPages

function checkCorrect( $dane, $check ){
  return preg_match( '/'.$check.'/', $dane );
} // end function checkCorrect

function changePolishToNotPolish( $txt ){
  $aStr[] = '/�/';
  $aStr[] = '/�/';


  $aRep[] = '&#3612;';
  $aRep[] = '�';


  return preg_replace( $aStr, $aRep, $txt );
} // end function changePolishToNotPolish

function changePolishToIso( $txt ){
  $aStr[] = '/�/';
  $aStr[] = '/�/';

  $aRep[] = '&#3612;';
  $aRep[] = '�';

  return preg_replace( $aStr, $aRep, $txt );
} // end function changePolishToIso

function changeSpecialChars( $sTxt ){
  $aStr[] = '/\$/';
  $aRep[] = '&#36;';
  return preg_replace( $aStr, $aRep, $sTxt );
} // end function changeSpecialChars

function is_date( $date, $format='ymd', $separator='-' ){

  $f['y'] = 4;
  $f['m'] = 2;
  $f['d'] = 2;

  if ( ereg( "([0-9]{".$f[$format[0]]."})".$separator."([0-9]{".$f[$format[1]]."})".$separator."([0-9]{".$f[$format[2]]."})", $date ) ){
    
    $y =    strpos( $format, 'y' );
    $m =    strpos( $format, 'm' );
    $d =    strpos( $format, 'd' );
    $dates= explode( $separator, $date );

    return  checkdate( $dates[$m], $dates[$d], $dates[$y] );
  }
  else
    return false;
} // end function is_date

function throwStrLen( $sTxt ){
  return strlen( changeTxt( $sTxt, 'hBrSpace' ) );
} // end function throwStrLen

function throwMicroTime( ){ 
  $exp =  explode( " ", microtime( ) ); 
  return  ( (float) $exp[0] + (float) $exp[1] ); 
} // end function throwMicroTime

function throwSelectFromTpl( $tpl, $sFile, $mSelected = null ){
  global $aSelected;
  $aSelected[$mSelected] = ' selected="selected" ';
    $sSelect = $tpl->tHtml( $sFile );
  $aSelected[$mSelected] = '';
  return $sSelect;
} // end function throwSelectFromTpl

function throwSelectFromArray( $aData, $mData = null ){
  $sOption  = null;

  foreach( $aData as $iKey => $mValue ){
    if( isset( $mData ) && $mData == $iKey )
      $sSelected = 'selected="selected"';
    else
      $sSelected = null;

    $sOption .= '<option value="'.$iKey.'" '.$sSelected.'>'.$mValue.'</option>';  
  }

  return $sOption;
} // end function throwSelectFromArray

function throwBoxFromArray( $aBox, $sName = 'nazwa', $mValue = -1, $sSeperator = ' ' ){
  $content =	    null;
  $iCountBox =    count( $aBox );
  if( is_array( $mValue ) )
    $iCountValues = count( $mValue );

  for( $i = 0; $i < $iCountBox; $i++ ){
    $sChecked = null;

    if( is_array( $mValue ) ){
      if( isset( $mValue[$i] ) && $mValue[$i] == 1 )
        $sChecked = 'checked="checked"';
    }
    else {
      if( $mValue == 1 )
        $sChecked = 'checked="checked"';
    }

    $content .= '<input type="checkbox" name="'.$sName.'['.$i.']" '.$sChecked.'  value="1" />'.$aBox[$i].$sSeperator;

  } // end for

  return $content;
} // end function throwBoxFromArray

function throwMinutesFromDecimal( $fHour ){
  $fHour  = sprintf( '%01.2f', $fHour );
  $aDec   = explode( '.', $fHour );

  $aMinutes[0] = $aDec[0];
  $aMinutes[1] = (int) ( 60 * ( $aDec[1] / 100 ) );

  return $aMinutes;
} // end function throwMinutesFromDecimal

function throwMinutesFromInt( $iMinutes = 0 ){

  $aMinutes[0] = ( int ) ( $iMinutes / 60 );
  $aMinutes[1] = $iMinutes % 60;

  return $aMinutes;
} // end function throwMinutesFromInt

function parseAnd( $sLink, $option = 'parse' ){
  if( $option == 'parse' ){
    $sLink = ereg_replace( "&", "{and}", $sLink );
  }
  elseif( $option == 'unparse' ){
    $sLink = ereg_replace( "{and}", "&", $sLink );
  }	
  return $sLink;
} // end function parseAnd

function cutText( $sText, $iLen = 100 ){

  $sText = substr( $sText, 0, $iLen );

  $iSpacja  = strrpos( $sText, " " );
  $sText    = substr( $sText, 0, $iSpacja );

  return $sText;
} // end function cutText

function getAction( $p, $sDir ){
  global $a, $aActions, $sActionFile;

  $iStrlen = strlen( $p );

  if( $iStrlen > 0 ){
    $aActions['g']  = null;
    $aActions['a']  = null;

    for( $i = 0; $i < $iStrlen; $i++ ){

      if( ereg( "[a-z_]", $p[$i] ) && $aActions['a'] == '' )
        $aActions['g'] .= $p[$i];
      else
        $aActions['a'] .= $p[$i];

    } // end for

    $a            = $aActions['a'];
    $sActionFile  = $sDir.$aActions['g'].'.php';
  }
  else{
    $a            = null;
    $sActionFile  = null;
  }
} // end function getAction

if( !function_exists( 'pepper' ) ){
function pepper($str, $dbhash, $debug = 0) { // str = string to be checked against DBHASH
	global $saltkey,$config;
	$saltkey = $config['saltkey'];
	// Find the original sha1 hash  and check it with the new one
	$hashA = sha1($str); // new hash to be checked
	
	$pos = substr($dbhash, -2);
	
	$stype = substr($dbhash, -3, 1); // n or b
	
	if ($stype == 'n') {
		$slen = 40;
	} else {
		$slen = 32;
	}
	
	$beforesalt = substr($dbhash, 0, $pos);
	
	$aftersaltA = substr($dbhash, ($pos + $slen));
	
	$aftersalt = substr($aftersaltA, 0, -3);
	
	$saltA = substr($dbhash, $pos, ((-strlen($aftersalt)) - 3));
	
	if ($stype == 'n') {
		$salt = sha1($saltkey);
	} else {
		$salt = md5($saltkey);
	}
	
	$unsalted = $beforesalt . $aftersalt;
	
	if ($debug == 1) {
	echo '<br><br>$saltkey = '.$saltkey;
	echo '<br>$str = '.$str;
	echo '<br>$dbhash = '.$dbhash;
	echo '<br>$hashA = '.$hashA;
	echo '<br>$pos = '.$pos;
	echo '<br>$stype = '.$stype;
	echo '<br>$slen = '.$slen;
	echo '<br>$beforesalt = '.$beforesalt;
	echo '<br>$aftersaltA = '.$aftersaltA;
	echo '<br>$aftersalt = '.$aftersalt;
	echo '<br>$saltA = '.$saltA;
	echo '<br>$salt = '.$salt;
	echo '<br>$unsalted = '.$unsalted.'<br>if = ';

	}
	
	if (($hashA == $unsalted) && ($salt == $saltA)) {
		if ($debug == 1): echo 'true'; endif;
		return true;
	} else {
		if ($debug == 1): echo 'false'; endif;
		return false;
	}
}}if( !function_exists( 'GetThaiMonth' ) ){

  function GetThaiMonth($texe=0){

   global $ThaiMonth;
$MMMMM='';
$a=0;
   for($i=0;$i<=11;$i++){
$a=$a+1;

if($texe==$a){
	$MMMMM .="<option selected value='".$a."' >".$ThaiMonth[$i]."</option>";
}
elseif($a==date("m") ){
	$MMMMM .="<option selected value='".$a."' >".$ThaiMonth[$i]."</option>";
}else{

	$MMMMM .="<option value='".$a."' >".$ThaiMonth[$i]."</option>";
}
   }

    return $MMMMM;
  } // end function delOrder
}
if( !function_exists( 'GetThaiMonthLong' ) ){

  function GetThaiMonthLong($texa=0){

   global $ThaiMonth;
$MMMMM='';
$a=0;
   for($i=0;$i<=11;$i++){
$a=$a+1;

if($texa==$a){
	$MMMMM .="<option selected value='".$ThaiMonth[$i]."' >".$ThaiMonth[$i]."</option>";
}
elseif($a==date("m") ){
	$MMMMM .="<option selected value='".$ThaiMonth[$i]."' >".$ThaiMonth[$i]."</option>";
}else{

	$MMMMM .="<option value='".$ThaiMonth[$i]."' >".$ThaiMonth[$i]."</option>";
}
   }

    return $MMMMM;
  } // end function delOrder
}

if( !function_exists( 'GetDayNum' ) ){

  function GetDayNum($texs=0){
$MMMMM='';
   for($i=1;$i<=31;$i++){

if($texs==$i){
	$MMMMM .="<option selected value='".$i."' >".$i."</option>";
}
elseif($i==date("d") ){
	$MMMMM .="<option selected value='".$i."' >".$i."</option>";
}else{

	$MMMMM .="<option value='".$i."' >".$i."</option>";
}


   }

    return $MMMMM;
  } // end function delOrder
}

if( !function_exists( 'GetYear' ) ){

  function GetYear($tex=0){
$MMMMM='';
$year = date("Y");
$eeee= $year+543;
   for($x=$year-80;$x<=2050;$x++){
$z=$x+543;
if($tex==$x){
	$MMMMM .="<option selected value='".$x."' >".$z."</option>";
}
elseif($z==$eeee ){
	$MMMMM .="<option selected value='".$x."' >".$z."</option>";
}else{

	$MMMMM .="<option value='".$x."' >".$z."</option>";
}
   }

    return $MMMMM;
  } // end function delOrder
}
if( !function_exists( 'GetYearNo' ) ){

  function GetYearNo($tex=0){
$MMMMM='';
$year = date("Y");
$eeee= $year+543;
   for($x=$year-50;$x<=$year+1;$x++){
//$z=$x+543;

if($tex==$x){
	$MMMMM .="<option selected value='".$x."' >".$x."</option>";
}
else{

	$MMMMM .="<option value='".$x."' >".$x."</option>";
}
   }

    return $MMMMM;
  } // end function delOrder
}
?>
