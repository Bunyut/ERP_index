<?php
require_once 'config/general.php';

Conn2DB();

$rssdession = session_id();
$ShowDateq = time();

if(isset($_GET["p"]) && ($_GET['p'] == 'logout')){
	session_destroy();
	session_unset();
	echo "<script>window.location='/login';</script>";
}

$workingCompany = $_SESSION['SESSION_Working_Company'];
$department = $_SESSION['SESSION_Department'];
$section = $_SESSION['SESSION_Section'];
$id_card = $_SESSION['SESSION_ID_card'];
$positionID = $_SESSION['SESSION_Position_id'];
if($workingCompany != "" && $department != "" && $section != "" && $id_card != "" && $positionID != ""){
	echo "<script>window.location='select_program.php';</script>";
}else{echo $tpl->tbHtml('login.html','LOGIN');}
?>
