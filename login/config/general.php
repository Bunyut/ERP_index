<?php
session_start();

$config['MASTER_DATA_PATH'] = dirname(dirname(dirname(__FILE__)))."/ERP_masterdata"; 
require_once $config['MASTER_DATA_PATH'].'/inc/main_function.php';
$database 	= new configAllDatabase();
$database->calConfigAllDatabase();

/* ตรวจสอบถ้าหากตั้งค่าเป็น DEMO */
$config['thisDemo'] = false;
if ($database->dbCh == 'DEMO') {
	$config['thisDemo'] = true;
}

require_once 'inc/TplParser.php';
require_once 'inc/TplFunction.php';
require_once 'inc/preferences.php';

$config['title']		= "ลงชื่อเข้าสู่ระบบ";

$config['ServerName']	= $database->con['ServerName'];
$config['UserName']		= $database->con['UserName'];
$config['UserPassword'] = $database->con['UserPassword'];
# - icchecktime
// $config['DataBaseName'] = "easyhr_icchecktime2"; 
// $config['DataBaseOU']   = "easyhr_OU";  
// $config['Member']		= "easyhr_icchecktime2";

$config['DataBaseName'] = $database->con['db_emp']; 
$config['DataBaseOU']   = $database->con['db_organi'];  
$config['Member']		= $database->con['db_emp'];

$config['saltkey']		= "momay2520pttamon";


$program['place_id'] = '28';// รหัสโปรแกรม  อ้างอิงจาก icchecktime2.place    12=โปรแกรมขายเดิม , 28=Easysaleโปรแกรมขาย  เวลาอัพขึ้น เซิฟจริง ให้ ตรวจสอบอีกครั้ง แต่ถ้าส่วนกลางส่งค่า GET มาแล้ว ค่าคอนฟิคนี้จะถูกเก็บไว้ใช้สำรองแทน
#- - -

$program['link_check_team'] = 'customers_management';//link เมนูเพื่อเช็กดึงข้อมูลทีม ว่าใครใช้เมนู การจัดการลูกค้ามุ่งหวังได้บ้าง]
$program['menu_check_team'] = 'จัดการลูกค้ามุ่งหวัง';

$user_config['AdminUser'] = array('admin','ปฏิภาณ','โสรญา','kenz','เบิร์ด','เอกชัย','เอกลักษณ์','ปิยะนุช','ธราธร');//username ของแอดมิน ใช้สำหรับเป็นข้อยกเว้นในการเข้าถึงส่วนที่จำกัดสิทธิ์ไว้ให้เข้าถึงทั้งหมด

# config เพื่อเชื่อมต่อฐานข้อมูลกลาง
$conf['CRM_REAL_PATH'] = dirname(dirname(dirname(__FILE__)))."/ERP_crm";

# -- TEMPLATE CLASS INITIAL
$tpl    = new TplParser;
$tpl->setDir('templates/');

if(!function_exists('Conn2DB')){
	function Conn2DB(){
		global $conn,$config;
		$conn = mysql_connect($config['ServerName'],$config['UserName'],$config['UserPassword'] );
		if (!$conn)die("ไม่สามารถติดต่อกับ ฐานข้อมูลได้ ".$config['ServerName'].$config['UserName'].$config['UserPassword'].mysql_error());
		if($config['DataBaseName']){
			mysql_select_db($config['DataBaseName'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้".mysql_error());
			@mysql_db_query($config['DataBaseName'], "SET NAMES UTF8");
		}
		if($config['DataBaseOU']){
			mysql_select_db($config['DataBaseOU'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้".mysql_error());
			@mysql_db_query($config['DataBaseOU'], "SET NAMES UTF8");
		}
	}
}

?>
