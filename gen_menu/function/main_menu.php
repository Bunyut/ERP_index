<?php
/**
 * Gen Menu Easy Sale (ใช้ได้กับโปรแกรมอื่นที่มีการสร้างแท๊กเมนูหลักเหมือนกับEasysale)
 * PT@2011-12-08
 * 2012-04 การเจนเมนูรองรับ jqWidjet เรียบร้อย
 */

$_SESSION['SESSION_menu_permission'] = array();

function generate_menu_ES($program_id,$DB_Member){return main_menu_cross($program_id, menu_permit($program_id,$DB_Member),$DB_Member);}

/**
 * menu_permit * ใช้คืนค่าidเมนูที่สามารถเข้าถึงสำหรับsessionที่ล็อคอินเข้ามา returnเป็นอาเรย์
 * #- pt@2011-12-07  recursive function
 */
function menu_permit($program_id,$DB_Member){
	global $db;
	if(!$_SESSION['SESSION_ID_card'] || !$_SESSION['SESSION_Position_id'] || !$_SESSION['SESSION_Working_Company'] || !$_SESSION['SESSION_Section'] || !$_SESSION['SESSION_Department']){

		if(!isAdmin()){
			#- กรณีนี้แสดงว่า ใช้ session ที่ไม่ได้มาจาก ICBoard ใหม่ และไม่ใช่ Admin -> user admin จะไม่มี session อื่นนอกจาก idcard จึงเปิดให้ยูสเซอร์ admin เข้าเห็นเมนู ชั่วคราว
			 session_destroy();
			$text_alert_incorrect = 'alert("การเข้าสู่ระบบไม่ถูกต้อง ข้อมูลบางส่วนไม่ครบถ้วน กรุณาล็อคอินใหม่..."); window.location = "login.php";';
			echo '	<script type="text/javascript">
					'.$text_alert_incorrect.'
				</script>'; 
		}else{
			//echo "ถึงผู้ดูแลระบบ : การเข้าสู่ระบบไม่ถูกต้อง ข้อมูลบางส่วนไม่ครบถ้วน หากมีการบันทึกข้อมูลใดๆ อาจทำให้ระบบประมวลผลผิดพลาดได้...";
			/*	$text_alert_incorrect = 'alert("ถึงผู้ดูแลระบบ : การเข้าสู่ระบบไม่ถูกต้อง ข้อมูลบางส่วนไม่ครบถ้วน หากมีการบันทึกข้อมูลใดๆ อาจทำให้ระบบประมวลผลผิดพลาดได้...");';
			}
			/*echo '	<script type="text/javascript">
			'.$text_alert_incorrect.'
			</script>';
			*/
		}
	}
	$index 		= array();
	$setpermit 	= array(); // เก็บเฉพาะไอดีเมนูที่บุคคลผู้นี้มีสิทธิ์เข้าถึงเท่านั้น (เก็บตั้งแต่เมนูหลักยันเมนูย่อยสุด)
	$parent_id 	= array();
	$overheat 	= 0;
	$num 		= 0;

	if(isAdmin()){
		$JOIN = " RIGHT JOIN ";//เอาทุกเมนูของโปรแกรมนี้
		$where = "";
	}else{
		$JOIN = "INNER JOIN";//เอาเฉพาะเมนูที่กำหนดสิทธิ์แล้ว และสิทธิ์นั้นก็มีตัวเองอยู่ในนั้นด้วย
		$where = " AND (menu_manage.working_company_id = '$_SESSION[SESSION_Working_Company]'
		OR menu_manage.id_card 				= '$_SESSION[SESSION_ID_card]'
		OR menu_manage.position_id 			= '$_SESSION[SESSION_Position_id]'
		OR menu_manage.section_id 			= '$_SESSION[SESSION_Section]'
		OR menu_manage.department_id 		= '$_SESSION[SESSION_Department]' )
		AND menu_manage.status != '99'  ";
	}

	$sql_permit = "SELECT DISTINCT(menu.id)
	FROM $DB_Member.menu_manage $JOIN $DB_Member.menu ON menu_manage.menu_id = menu.id
	WHERE menu.place_id = '$program_id' AND menu.status != '99' $where ";

	$res_permit = mysql_query($sql_permit)or die('<br>Error Line:'.__LINE__.' '.mysql_error());
	//	echo $sql_permit;
	while($fet_permit = mysql_fetch_assoc($res_permit)){
		$findMain_boolean = true;
		//$menuidsub = $fet_permit['menu_id']; //จำเป็นต้องใช้ $menuidsub
		$menuidsub = $fet_permit['id']; //จำเป็นต้องใช้ $menuidsub
		if(!(in_array($menuidsub, $setpermit))){
			$setpermit[] = $menuidsub;
		}
		while($findMain_boolean){ // เก็บ id ของเมนูparent ไว้ใน $setpermit ด้วย เพราะตอนตั้งค่าไม่สามารถกำหนดให้เมนูหลักได้
			$sql_parent = "SELECT parent_id FROM $DB_Member.menu WHERE id =  $menuidsub AND status != '99'";
			$res_parent = mysql_query($sql_parent)or die('<br>Error Line:'.__LINE__);
			$fet_parent = mysql_fetch_assoc($res_parent);
			if($fet_parent['parent_id'] == ''){
				$findMain_boolean = false;
			}else{
				$menuidsub = $fet_parent['parent_id'];
				if(!(in_array($menuidsub, $setpermit))){
					$setpermit[] = $menuidsub;
				}
				$findMain_boolean = true;
			}
			if($overheat>1000){echo '<br>OVER HEAT  : '.$sql_permit.'<br>'.$sql_parent;} //กันเหนียว
			$overheat++;
		}
	}
	//	echo '<br>';print_r($setpermit);

	return  $setpermit;
}



/**
 * main_menu * Gen main menu เมนูหลักของโปรแกรม
 * return $GEN = แท็กเมนูเป็น HTML ในรูปแบบที่ใช้กับeasysaleได้
 * pt@2011-12-01
 * Last Update :  pt@2012-02-10
 */
function main_menu_cross($program_id,$menu_permittion = '',$DB_Member ){
	global $db,$conf;
	$ShowDateq=time();
	$idcard = $_SESSION['idcard'];
	$sign = '';
	for($i=0;$i<count($menu_permittion);$i++){ //เปลี่ยนอาร์เรย์ให้อยู่ในรูปของ SQL IN()
		$in .= $sign."'".$menu_permittion[$i]."'";	$sign = ",";
	}

	if($in){
		$GEN = array();
		$GEN['MainMenu'] = $GEN['Sub'] = '';
		$numSub = 0;
		$SubMenu = array();
		
		$db[Member] = $DB_Member;
		$sql = "SELECT id, place_id, name, menu_image, link, parent_id, rankking, process_id, status FROM $db[Member].menu
				WHERE place_id = '$program_id' AND id IN ($in) AND status!= '99' AND parent_id = '' ORDER BY rankking ASC ";
		$res = mysql_query($sql)or die('<br>Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error(). '  '.$sql);
		while($fet = mysql_fetch_assoc($res)){
			#-ชื่อเมนูหลัก เก็บครั้งเดียว
			//ตอนนี้กำหนดให้เมนูหลักไม่มีการ ลิ๊ง
			//$GEN['MainMenu'] .= '<li><a onclick = "javascript:browseCheck(\'main\',\''.$url.'\',\'\');" href="javascript:void(0);" title="'.$fet[name].'" rel="ddsubmenu'.($numSub+1).'">'.$fet[name].'</a></li>';
			$GEN['MainMenu'] .= '<li style="height:15px;"><a title="'.$fet[name].'" ><b>'.$fet[name].'</b></a> <ul  style="width: 250px;">  $GEN[Sub_'.$numSub.']  </ul> </li>';

			$sql_findSub = "SELECT id FROM $db[Member].menu WHERE place_id = '$program_id' AND parent_id = '$fet[id]' AND status != '99' ";
			$res_findSub = mysql_query($sql_findSub)or die('Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error());
			if(mysql_num_rows($res_findSub)){ //ถ้ามีลูก  // กรณีนี้ยังไงก็มี
				//$SubMenu[$numSub] =	sub_menu_1($program_id, $fet['id']);
				$GEN['Sub_'.$numSub] = sub_menu_1($program_id, $fet['id'], $in);
				$numSub++;
			}else{//กรณีไม่มีเมนูย่อยลงมาจากเมนูหลัก, ซึ่งไม่มีทางเข้าเพราะ$menu_permittion จะคัดเอาเมนูที่ ไม่มีสิทธิ์ เข้าถึงออกไปหมดแล้ว
				//PT@2012-03-30  OU  เจนเมนูที่ยังไม่มี Sub  ถ้าไม่มี ตรงนี้ จะเจน ผิดพลาด เมนูย่อยจะมั่ว เลย ต้องมี $numSub++
				$GEN['Sub_'.$numSub] = '<li title="Admin:กรุณาติดต่อผู้ดูแลระบบ" onclick="javascript:UnLoadMap();"><a>ไม่พบเมนูย่อย</a></li>';
				$numSub++;
			}
		}
	}else{
		$GEN['MainMenu'] = '<li><a style="color:white; background-color:"> คุณไม่สามารถใช้งานเมนูต่างๆของ '.$conf['title'].' ครับ </a></li>';
	}
	return $GEN;
}


/**
 * sub_menu_1 * Gen sub menu ชั้นลึกลงไปได้เรื่อยๆ (recursive)
 * pt@2011-12-01
 */
function sub_menu_1($program_id, $menu_id, $in){//pt recursive function
	global $db,$conf;
	$SubMenu1 = $SubMenu2 = '';

	#- ตัวแปรที่มีอยู่ใน $url --------------#
	$ShowDateq=time();
	$rssdession= session_id();
	#---------------------------------#

	$sql = "SELECT menu.id, menu.place_id, menu.name, menu.menu_desc, menu.menu_image, menu.link, menu.parent_id, menu.status, menu_process.process, menu.process_id
			FROM $db[Member].menu JOIN $db[Member].menu_process ON menu.process_id = menu_process.id
			WHERE menu.place_id = '$program_id' AND menu.id IN ($in) AND menu.status!= '99' AND menu.parent_id = '$menu_id' ORDER BY menu.rankking ASC ";
	$res = mysql_query($sql)or die('Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error());
	while($fet = mysql_fetch_assoc($res)){

		$sql_skill = "SELECT menu_manage.skill FROM $db[Member].menu_manage WHERE menu_manage.status != '99' AND menu_manage.menu_id = '$fet[id]'";
		$res_skill = mysql_query($sql_skill)or die('Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error());
		$fet_skill = mysql_fetch_assoc($res_skill);

			#echo '<br>'.$fet['process'].' permission='.$fet_skill['skill'].' menu id='.$fet[id].' : '.$fet[name].'  | process_id:'.$fet['process_id'];
		$url = $fet['link'];
		$PROCESS = $fet['process']; //เซต $url เข้าไป
		//if($fet['process_id'] != '5' && $url !=''){#!!! ทำไมต้อง != 5
		if($url !=''){	//Zan@2011-12-12
			$PROCESS = str_replace('\"', '"', $PROCESS); // \" => "
			$PROCESS = str_replace('"', '\"', $PROCESS); // " => \"   กรณีที่กำหนดใน process ไม่เหมือนกัน เจอกับ pop up ไม่เหมือน browsecheck

			//ZAN@2012-02-22 sent get page session fo check_per_page
			if( !empty($conf['check_permission']) ){//ถ้าเปิดใช้งานถึงจะเริ่ม ใช้กับฟังก์ชั่น check_access_permission()
				$url_key = md5($url);
				$check_p = explode('.php',$url);
				$check_page = $check_p[0].'.php';
				$_SESSION['SESSION_menu_permission'][$check_page][$url_key] = $fet['id'];//เก็บไว้เช็กหน้าต่อหน้า
				if(substr($check_p[1],0,1)!='?' || $check_p[1]==''){$url.='?';}
				$url .= "$conf[check_permission]=$url_key&$conf[check_main_menu]=1";//main_menu ส่งเพื่อให้รู้ว่าเปิดเช็กสิทธิ์
			}
			//--
			$url.='&t='.$ShowDateq.'&menu_name='.$fet['name'].'&menu_desc='.$fet['menu_desc'];
			eval('$url = "'.$url.'";');
			eval('$PROCESS = "'.$PROCESS.'";');  ###########-- ถ้าเกิด Error บรรทัดนี้ แสดงว่า มีเมนูบางเมนูไม่ได้ตั้งค่า process ให้เป็น browcheck ให้ไปตั้งค่าใน easyhr ให้เรียบร้อย

			//test
			if($fet['process_id']=='3' && $testttttt){
				echo '::Check Pop Up ERROR::<br><textarea cols=150 rows=10>';
				echo "\n\n \$fet[link] :: ".$fet['link'];
				echo "\n\n EVAL('\$url = \"\$fet[link]\";')\n\$url = ".$url;
				echo "\n---------------------------------------------------------------------------------------------------";
				echo "\n \$fet[process] :: ".$fet['process'];
				echo "\n\n EVAL('\$PROCESS = \"\$fet[process]\";')\n\$PROCESS = ".$PROCESS.'</textarea>';
			}
		}//if
		else{  //ถ้าไม่มีลิงค์
			//$PROCESS = '<a href="javascript:void(0);">';
			$PROCESS = '<a>';
		}

		#------------------------------------2012-04-12
		//clean $PROCESS for jQwidget by PT
		$PROCESS = str_replace('href="javascript:void(0);"','',$PROCESS);
		$PROCESS = str_replace('onClick','onclick',$PROCESS);
		//$PROCESS = str_replace('javascript:','',$PROCESS);
//echo "\n".$PROCESS ;
		//onclick on li
		$PROCESS_similarly =  str_replace('<a','',str_replace('>','',$PROCESS));
		$PROCESS = '<a>';
		#------------------------------------end2012-04-12

		$tag_a = '</a>';

		//echo '<br>::'.$test.'</a><br>';
		#echo '<br><textarea cols=100>'.$PROCESS.'</textarea>';
		$seperate_line = '<li type="separator"></li>'; //เส้นแบ่งแต่ละเมนู  เอาไปแทรก ตรงที่ $SubMenu1  แล้ว จะมีเส้นแบ่งเมนูตอนเจนเมนู  2012-04-19 เปลี่ยนเป็น ไม่ใส่เส้น ^^PT
		$sql_findSub = "SELECT id FROM $db[Member].menu WHERE place_id = '$program_id' AND parent_id = '$fet[id]' AND status != '99' ";
		$res_findSub = mysql_query($sql_findSub)or die('Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error());
		$SubMenu2='';
		if(mysql_num_rows($res_findSub)){ //ถ้ามีลูก
			$SubMenu2 =	sub_menu_1($program_id, $fet['id'],$in);
			$SubMenu1 .= "<li title=\"\" >".$PROCESS.$fet['name'].$tag_a."<ul style=\"width: 250px;\">".$SubMenu2."</ul></li>";
		}else{ //ถ้าไม่มีลูก
		
			/***************************************************
			*
			*		golf edit 27/06/2012
			*		เปลี่ยน action ที่จะ direct ไปทำที่ tag a จากเดิมทำที่ tag li 
			*		เพื่อจะรองรับ การ direct ไปยัง iframe ซึ่งต้องอาศัย attr href ของ tag a
			*
			****************************************************/		
			$SubMenu1 .= '<li title="'.$fet['menu_desc'].'" ><a '.$PROCESS_similarly.' >'.$fet['name'].$tag_a.'</li>';
			#--	end golf edit
			
			#$SubMenu1 .= '<li title="'.$fet['menu_desc'].'" '.$PROCESS_similarly.'>'.$PROCESS.$fet['name'].$tag_a.'</li>';
			
		}
	}
	#echo 'sample:'.htmlspecialchars($SubMenu1).'<br>';
	return $SubMenu1;
}

/**
 * Main Directory * ใช้คืนค่าตำแหน่งเมนู เป็นอาเรย์
 * #- pt@2011-12-01  recursive function
 */
function main_directory($program_id, $menu_id = ''){
	global $db;
	$index = array(); $num = 0;
	$sql = "SELECT id, name, link FROM $db[Member].menu WHERE place_id = '$program_id' AND status!= '99' AND parent_id = '$menu_id' ORDER BY rankking ASC ";
	$res = mysql_query($sql)or die('Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error());
	while($fet = mysql_fetch_assoc($res)){
		if($num >=  500){alert('OVER HEAT'); exit();}
		$sql_findSub = "SELECT id FROM $db[Member].menu WHERE place_id = '$program_id' AND parent_id = '$fet[id]' AND status != '99' ";
		$res_findSub = mysql_query($sql_findSub)or die('Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error());
		if(mysql_num_rows($res_findSub)){
			$index[$num] = main_directory($program_id, $fet['id']);
		}
		else {
			$index[$num] = $fet['id'];
		}
		$num++;
	}
	return $index;
}
if(!function_exists('isAdmin')){
/**
*
*isAdmin() return false
*
*/

function isAdmin(){
return false;//ยังไม่ได้เปิดใช้กับโปรแกรมอื่น  โปรแกรมขายใช้เช็กแอดมิน
}
}
//FULL isAdmin()
//if(!function_exists('isAdmin')){
	/**
	 *  isAdmin() เช็คว่าเข้ารหัสเป็น admin หรือไม่
	 *
	 *  @param ชื่อล็อคอิน(username) defualt = blank
	 *
	 * @return true=เป็นadmin  ,  false=ไม่ใช่admin
	 */
	//function isAdmin($username = ''){
	//	global $user_config;
	//	$username = ($username == '') ? $_SESSION['SESSION_username'] : $username;
	//	return in_array( $username , $user_config['AdminUser'] );
	//}
//}



/**
 * main_menu * Gen main menu เมนูหลักของโปรแกรม
 * return $GEN = แท็กเมนูเป็น HTML ในรูปแบบที่ใช้กับeasysaleได้
 * pt@2011-12-01
 * Last Update :  pt@2012-02-10
 */
/*
 function main_menu($program_id,$menu_permittion = ''){
 global $db,$conf;
 $ShowDateq=time();
 $idcard = $_SESSION['idcard'];
 $sign = '';
 for($i=0;$i<count($menu_permittion);$i++){ //เปลี่ยนอาร์เรย์ให้อยู่ในรูปของ SQL IN()
 $in .= $sign."'".$menu_permittion[$i]."'";	$sign = ",";
 }

 if($in){
 $GEN = array();
 $GEN['MainMenu'] = $GEN['Sub'] = '';
 $numSub = 0;
 $SubMenu = array();

 $sql = "SELECT id, place_id, name, menu_image, link, parent_id, rankking, process_id, status FROM $db[Member].menu
 WHERE place_id = '$program_id' AND id IN ($in) AND status!= '99' AND parent_id = '' ORDER BY rankking ASC ";
 $res = mysql_query($sql)or die('<br>Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error(). '  '.$sql);
 while($fet = mysql_fetch_assoc($res)){
 #-ชื่อเมนูหลัก เก็บครั้งเดียว
 //ตอนนี้กำหนดให้เมนูหลักไม่มีการ ลิ๊ง
 //$GEN['MainMenu'] .= '<li><a onclick = "javascript:browseCheck(\'main\',\''.$url.'\',\'\');" href="javascript:void(0);" title="'.$fet[name].'" rel="ddsubmenu'.($numSub+1).'">'.$fet[name].'</a></li>';
 $GEN['MainMenu'] .= '<li><a  href="javascript:void(0);" title="'.$fet[name].'" rel="ddsubmenu'.($numSub+1).'">'.$fet[name].'</a></li>';

 $sql_findSub = "SELECT id FROM $db[Member].menu WHERE place_id = '$program_id' AND parent_id = '$fet[id]' AND status != '99' ";
 $res_findSub = mysql_query($sql_findSub)or die('Error on File:'.__FILE__.' Line:'.__LINE__.mysql_error());
 if(mysql_num_rows($res_findSub)){ //ถ้ามีลูก  // กรณีนี้ยังไงก็มี
 //$SubMenu[$numSub] =	sub_menu_1($program_id, $fet['id']);
 $GEN['Sub_'.$numSub] = sub_menu_1($program_id, $fet['id'], $in);
 $numSub++;
 }else{//กรณีไม่มีเมนูย่อยลงมาจากเมนูหลัก, ซึ่งไม่มีทางเข้าเพราะ$menu_permittion จะคัดเอาเมนูที่ ไม่มีสิทธิ์ เข้าถึงออกไปหมดแล้ว
 //PT@2012-03-30  OU  เจนเมนูที่ยังไม่มี Sub  ถ้าไม่มี ตรงนี้ จะเจน ผิดพลาด เมนูย่อยจะมั่ว เลย ต้องมี $numSub++
 $GEN['Sub_'.$numSub] = '<li title="Admin:กรุณาติดต่อผู้ดูแลระบบ" onclick="javascript:UnLoadMap();"><a>ไม่พบเมนูย่อย</a></li>';
 $numSub++;
 }
 }
 }else{
 $GEN['MainMenu'] = '<li><a class="menulink" style="color:white; background-color:green"> คุณไม่สามารถใช้งานเมนูต่างๆของ '.$conf['title'].' ครับ </a></li>';
 }
 return $GEN;
 }
 */

?>